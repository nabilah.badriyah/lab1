from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'Nabilah' # TODO Implement this

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(2000)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    hariIni = date.today()
    return hariIni.year-birth_year
